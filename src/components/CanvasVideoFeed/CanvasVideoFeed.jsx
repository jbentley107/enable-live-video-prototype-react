/**
 * @file CanvasVideoFeed.js
 */
import * as React from 'react'
import PropTypes from 'prop-types'

const CanvasVideoFeed = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  return (
    <Tag className={`canvas-video-feed canvas-video-feed--${variant} ${className}`}>
      {children}
      <style jsx>{`
        .canvas-video-feed {

        }
      `}</style>
    </Tag>
  )
}

CanvasVideoFeed.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

CanvasVideoFeed.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default CanvasVideoFeed;

<html>
  <head>
    <script type="text/JavaScript">
      var url = "Traffic.jpg"; //url to load image from
      var refreshInterval =1000;//in ms
      var drawDate =true;//draw date string
      var img;
      function init(){
        var canvas =document.getElementById("canvas");
        var context = canvas.getContext("2d");    
        img = new Image();    
        img.onload =function(){
          canvas.setAttribute("width", img.width);
          canvas.setAttribute("height", img.height);        
          context.drawImage(this,0,0);

          if (drawDate){
            var now =newDate();
            var text= now.toLocaleDateString() + " "+ now.toLocaleTimeString();
            var maxWidth =100;
            var x = img.width-10-maxWidth;
            var y = img.height-10;            
            context.strokeStyle ='black';            
            context.lineWidth =2;            
            context.strokeText(text, x, y, maxWidth);            
            context.fillStyle ='white';            
            context.fillText(text, x, y, maxWidth);
          }
        };    
        refresh();}
      function refresh(){    
        img.src = url +"?t="+newDate().getTime();
        setTimeout("refresh()",refreshInterval);
      }
    </script>
    <title>Traffic Camera Example</title>
    </head>
    <body onload="JavaScript:init();">
      <canvas id="canvas"/>
    </body>
  </html>