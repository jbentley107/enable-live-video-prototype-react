import React from 'react';
import logo from './logo.svg';
import './App.css';

import VideoFeed from './components/VideoFeed';

function App() {
  return (
    <div className="App">
      <VideoFeed />
    </div>
  );
}

export default App;
